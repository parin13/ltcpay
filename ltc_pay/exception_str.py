class UserExceptionStr():
    """
    Raw Data for End User Information/Exception
    """
    some_error_occurred = 'Some Error Occurred'
    bad_request = 'Bad Request!'
    specify_required_fields = 'Please specify all required fields!'
    input_params_wrong = 'Input Parameters are wrong.'
    insufficient_funds_ether = 'Insufficient funds in Ether'
    insufficient_tokens = 'Insufficient tokens in account'
    invalid_user = 'Invalid User'
    not_user_address = 'This Address does not correspond to the User'
    nodejs_server_down = 'Node JS Server is Down'
    enc_error = 'Encryption Error'
    wrapcore_exception = 'litecoin server not responding'
    address_exception = 'invalid address'
    invalid_amount = 'amount should be greater than 0'
    invalid_balance = 'insufficent balance'