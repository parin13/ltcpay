import models
import exception_str
import custom_exception
import common_util
import requests
import json
import sys, os
from decimal import Decimal

# Log
log = 'end_points'
logs_directory, category = common_util.get_config(log)

# Common Methods for eth and erc
obj_common = common_util.CommonUtil(log=log)


def get_fee(user_name, token):
    """
    Get Fee
    :return:
    """
    try:

        # Node URL
        node_url = common_util.config.get('node','url')

        # Get Network Fee URL
        response = obj_common.rpc_request(node_url, 'estimatesmartfee', [2])
        print response
        if not response:
            raise Exception('RPC Error')

        network_fee = round(float(response.get('result')["feerate"]), 8)

        # Recommended Fee
        recommended_fee = round((network_fee + (network_fee * 3) / 100), 8)

        return network_fee, recommended_fee

    except Exception as e:
        print e
        obj_logger = common_util.MyLogger(logs_directory, category)
        exc_type, exc_obj, exc_tb = sys.exc_info() 
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1] 
        error_stack = str(exc_type) + str(fname) + str(exc_tb.tb_lineno)
        obj_logger.error_logger('Error get_balance : ' + str(error_stack))
        obj_logger.error_logger('Error get_fee : ' + str(e))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)


def get_balance(address):
    """
    To Get user Balance
    """
    try:

        # Logs
        obj_logger = common_util.MyLogger(logs_directory, category)


        # Find in SQL aaddress present or not
        sql_data_ = models.find_sql(logger=obj_logger, table_name='address_master', filters= {'address':address})

        if not sql_data_:
            obj_logger.msg_logger('address not found in database : ')
            raise Exception ('data not found')

        # Find in SQL
        sql_data = models.find_sql(logger=obj_logger, table_name='transaction_master', filters= {'to_address':address, 'spent_flag': False})

        # Calculate Balance
        confirmed_balance = 0.0
        unconfirmed_balance = 0.0     

        # if not sql_data:
        #     obj_logger.msg_logger('data not found in  sql : ')
        #     raise Exception ('data not found')




        if sql_data:

            for data in sql_data:
                confirmation = int(data["confirmations"])
                value = float(data["value"])
                if confirmation == 0:
                    unconfirmed_balance = Decimal(unconfirmed_balance) + Decimal(value)
                else:
                    confirmed_balance = Decimal(confirmed_balance) + Decimal(value)

        return {"address":address, "confirmed_balance":float(confirmed_balance), "unconfirmed_balance":float(unconfirmed_balance)}

    except Exception as e:
        print e
        exc_type, exc_obj, exc_tb = sys.exc_info() 
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1] 
        error_stack = str(exc_type) + str(fname) + str(exc_tb.tb_lineno)
        obj_logger.error_logger('Error get_balance : ' + str(error_stack))
        obj_logger.error_logger('Error get_balance : ' + str(e))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)






# def forward_ltc():
#     """
#     Forward LTC
#     :return:
#     """
#     pass


def validate_address(address):
    """
    To Validate Address
    :param address: address
    :return:
    """
    try:
        # Logs
        obj_logger = common_util.MyLogger(logs_directory, category)

        # Node URL
        node_url = common_util.config.get('node', 'url')
        valid_address  = obj_common.rpc_request(url=node_url,method="validateaddress",params=[address])
        if not valid_address.get('result',{}).get("isvalid",False):
            raise Exception(exception_str.UserExceptionStr.address_exception)

    except custom_exception.UserException as e:
        print e
        raise custom_exception.UserException(exception_str.UserExceptionStr.wrapcore_exception)
    except Exception as e:
        print e
        obj_logger.error_logger('Error get_balance : ' + str(e))
        raise custom_exception.UserException(exception_str.UserExceptionStr.address_exception)


def generate_address(user_name, token):
    """
    To generate address
    :param user_name:
    :param token:
    :return:
    """
    try:
        # Logs
        obj_logger = common_util.MyLogger(logs_directory, category)

        # Get Address
        url = common_util.config.get('nodejs', 'address')
        response = requests.get(url)
        data = json.loads(str(response.text))

        if not data:
            raise custom_exception.UserException(exception_str.UserExceptionStr.nodejs_server_down)

        address = data.get('address')
        private_key = data.get('private_key')
        public_key = data.get('public_key')

        # Encrypt Private key
        enc_private_key = common_util.AESCipher(token, log).encrypt(private_key)
        dec_private_key = common_util.AESCipher(token, log).decrypt(enc_private_key)

        # Encryption Test
        enc_pass = (private_key == dec_private_key)

        if not enc_pass:
            raise custom_exception.UserException(exception_str.UserExceptionStr.enc_error)

        inserted = models.insert_sql(obj_logger, table_name='address_master', data={
            'user_name' : user_name,
            'private_key': enc_private_key,
            'public_key': public_key,
            'address': address
        })

        if not inserted:
            raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)

        # Add in Redis for crawlers
        obj_common.get_redis_connection().sadd('aw_set',address)

        return address

    except custom_exception.UserException:
        raise
    except Exception as e:
        obj_logger.error_logger('Error generate_address : ' + str(e))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)


 
def create_raw_transaction(from_address,output_address_json):    
    try:        
        # Logs
        obj_logger = common_util.MyLogger(logs_directory, category)
        # Node URL
        node_url = common_util.config.get('node', 'url')
        #get txids and vout of given addresses

        sql_data = models.find_sql(logger=obj_logger, table_name='transaction_master', filters= {'to_address':from_address,'spent_flag':False})

        #syntax of create raw transction   --> [{"txid":txid,"vout":n},...] {address:amount,...}
        # data_array --> [{"txid":txid,"vout":n},...]
        #{address:amount,...} --> tra
        data_array = []
        for data in sql_data:
            temp = {}
            if int(data["confirmations"]) > 0:
                temp["txid"] =  data["tx_id"]
                temp["vout"] =  int(data["vout"])
                data_array.append(temp)
        raw_transaction_response = obj_common.rpc_request(url=node_url,method = 'createrawtransaction',params = [data_array,output_address_json])
        raw_transaction = str(raw_transaction_response.get('result'))
        return raw_transaction,data_array
    # def rpc_request(self, url, method, params):
# [json.loads(vin),json.loads(vout)]
    except Exception as e:
        print e
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + str(fname) + str(exc_tb.tb_lineno)
        print error_stack
        obj_logger.error_logger('createrawtransaction error : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)


 
def sign_raw_transaction(raw_transaction_hex,private_key):    
    try:        
        # Logs
        obj_logger = common_util.MyLogger(logs_directory, category)
        # Node URL
        node_url = common_util.config.get('node', 'url')
        #get txids and vout of given addresses

        sign_raw_transaction = obj_common.rpc_request(url=node_url,method = 'signrawtransaction',params = [raw_transaction_hex,[],[private_key]])
        raw_transaction = str(sign_raw_transaction["result"]["hex"])
        return raw_transaction

    except Exception as e:
        print e
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + str(fname) + str(exc_tb.tb_lineno)
        print error_stack
        obj_logger.error_logger('signrawtransaction error : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)



def send_raw_transaction(signed_transaction):    
    try:        
        # Logs
        obj_logger = common_util.MyLogger(logs_directory, category)
        # Node URL
        node_url = common_util.config.get('node', 'url')
        #get txids and vout of given addresses

        txid_resp = obj_common.rpc_request(url=node_url,method = 'sendrawtransaction',params = [signed_transaction])
        txid = str(txid_resp["result"])
        return txid

    except Exception as e:
        print e
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + str(fname) + str(exc_tb.tb_lineno)
        print error_stack
        obj_logger.error_logger('signrawtransaction error : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request) 

    # status = update_sql(obj_logger, 'transaction_master', {'tx_id': tx_id},updated_values={'confirmations': confirmations, 'block_number': block_number})


def update_spent_txids(spent_tx_ids):    
    try:        
        # Logs
        obj_logger = common_util.MyLogger(logs_directory, category)
        for data in spent_tx_ids:
            status = models.update_sql(obj_logger, 'transaction_master', {'tx_id':str(data["txid"])},updated_values={'spent_flag': 1})

    except Exception as e:
        print e
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + str(fname) + str(exc_tb.tb_lineno)
        print error_stack
        obj_logger.error_logger('update_spent_txids error : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)


def validate_amount(address,value):
    try:
        obj_logger =  common_util.MyLogger(logs_directory,category)
        amount_data =  models.find_sql(logger= obj_logger,table_name='transaction_master',filters = {'to_address':address})
        if float(value) <= 0.0:
            raise custom_exception.UserException(exception_str.UserExceptionStr.invalid_amount)
        confirmed_balance = 0.0
        for data in amount_data:
            if int(data["confirmations"]) > 1:
                confirmed_balance = confirmed_balance +float(data["value"])
        if float(value) >= float(confirmed_balance):
            print float(value)
            print float(confirmed_balance)
            raise custom_exception.UserException(exception_str.UserExceptionStr.invalid_balance)

    except Exception as e:
        print e
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + str(fname) + str(exc_tb.tb_lineno)
        print error_stack
        obj_logger.error_logger('validate_amount : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)

        sql_data_ = models.find_sql(logger=obj_logger, table_name='address_master', filters= {'address':address})

def key(address):
    try:
        obj_logger =  common_util.MyLogger(logs_directory,category)
        key = models.find_sql(obj_logger,table_name = 'address_master',filters={'address':address})
        key_ = key[0]
        return key_["private_key"]
    
    except custom_exception.UserException:
        raise

    except Exception as e:
        print e
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + str(fname) + str(exc_tb.tb_lineno)
        print error_stack
        obj_logger.error_logger('validate_amount : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)