"""ltc_pay URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from . import views

urlpatterns = [
    url(r'generate_address/', views.generate_address, name='genrate_address'),
    url(r'get_fee/', views.get_fee, name='get_fee'),
    url(r'get_balance/', views.get_balance, name='get_balance'),
    url(r'batch_forward_ltc/', views.batch_forward_ltc, name='batch_forward_ltc'),
    url(r'peer_forward_ltc/', views.peer_forward_ltc, name='peer_forward_ltc')
]
