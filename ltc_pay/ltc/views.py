# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import exception_str
import custom_exception
import common_util
import util
import json
import configparser
obj_common = common_util.CommonUtil(log=util.log)

@csrf_exempt
@obj_common.who_is_hitting
@obj_common.valid_user
def generate_address(request):
    """
    End point for Generating Address
    :param request: user_name, token
    :return:
    """
    if request.method == 'POST':
        try:
            # Get Request Data
            post_data = json.loads(request.body)
            user_name = post_data.get("user_name", '')
            token = post_data.get("token", '')

            # Server Side Checks
            common_util.check_if_present(user_name, token)

            # Generate address
            address = util.generate_address(user_name, token)

            return JsonResponse({'address': str(address), 'status': 200})

        except custom_exception.UserException as e:
            return JsonResponse({'error': str(e), 'status': 400})
        except Exception as e:
            obj_logger = common_util.MyLogger(util.logs_directory, util.category)
            obj_logger.error_logger('generate_address : %s' % (str(e)))
            return JsonResponse({'error': exception_str.UserExceptionStr.bad_request, 'status': 400})

@csrf_exempt
@obj_common.who_is_hitting
@obj_common.valid_user
def get_fee(request):
    """
    Get Transaction Fee
    :param request: user_name, token
    :return:
    """
    if request.method == 'POST':
        try:
            # Get Request Data
            post_data = json.loads(request.body)
            user_name = post_data["user_name"]
            token = post_data["token"]

            # Server Side Checks
            common_util.check_if_present(user_name, token)

            network_fee,recommended_fee  = util.get_fee(user_name, token)
            return JsonResponse({"network_fee": network_fee, "recommended_fee": recommended_fee, "status" : 200})

        except custom_exception.UserException as e:
            return JsonResponse({'error': str(e), 'status': 400})
        except Exception as e:
            obj_logger = common_util.MyLogger(util.logs_directory, util.category)
            obj_logger.error_logger('get_fee : %s' % (str(e)))
            return JsonResponse({'error': exception_str.UserExceptionStr.bad_request, 'status': 400})


@csrf_exempt
@obj_common.who_is_hitting
@obj_common.valid_user
def get_balance(request):
    """
    Get Balance
    :param request: user_name, token
    :return:
    """
    if request.method == 'POST':
        try:
            # Get Request Data
            post_data = json.loads(request.body)
            user_name = post_data["user_name"]
            token = post_data["token"]
            address = post_data["address"]

            # Server Side Checks
            common_util.check_if_present(user_name, token, address)

            # Validate Address
            util.validate_address(address)

            # Get Balance
            balance_json = util.get_balance(address)

            balance_json["status"] = "200"
            return JsonResponse(balance_json)
        except custom_exception.UserException as e:
            return JsonResponse({'error': str(e), 'status': 400})
        except Exception as e:
            obj_logger = common_util.MyLogger(util.logs_directory, util.category)
            obj_logger.error_logger('get_balance : %s' % (str(e)))
            return JsonResponse({'error': exception_str.UserExceptionStr.bad_request, 'status': 400})


@csrf_exempt
@obj_common.who_is_hitting
@obj_common.valid_user
def batch_forward_ltc(request):
    # Node URL
    from_address = common_util.config.get('forward_balance', 'from_address')
    
    """
    Write Proper comments - Parin
    :param request: user_name, token
    :return:
    """
    if request.method == 'POST':
        try:
            # Get Request Data
            post_data = json.loads(request.body)
            user_name = post_data["user_name"]
            token = post_data["token"]
            output_address_json = post_data["output_address"]
            # Server Side Checks
            common_util.check_if_present(user_name, token, output_address_json)

            send_amount = 0.0

            # for data_json in transaction_array:
            for key, value in output_address_json.iteritems():
                # Validate Address
                util.validate_address(key)
                #amount calculation
                send_amount =  send_amount + float(value)


            if float(send_amount) <= 0.0 :
                return JsonResponse({'error': exception_str.UserExceptionStr.invalid_amount, 'status': 400})

            available_balance = util.get_balance(from_address)
            confirmed_balance =  float(available_balance.get('confirmed_balance'))
            
            if confirmed_balance < float(send_amount):
                return JsonResponse({'error': exception_str.UserExceptionStr.invalid_balance, 'status': 400})

            remaning_balance = confirmed_balance - float(send_amount)

            network_fee, recommended_fee = util.get_fee(user_name, token)
            remaning_balance = float(remaning_balance - float(recommended_fee))
            output_address_json[from_address] = remaning_balance

            raw_transaction_hex,spent_tx_ids = util.create_raw_transaction(from_address,output_address_json)
            private_key = common_util.config.get('forward_balance', 'private_key')  
            log = 'end_points'
            dec_private_key = common_util.AESCipher(token, log).decrypt(private_key)      
            signed_transaction = util.sign_raw_transaction(raw_transaction_hex,dec_private_key)
            txid =  util.send_raw_transaction(signed_transaction)
            util.update_spent_txids(spent_tx_ids)

            return JsonResponse({'txid': str(txid), 'status': 200})

        except custom_exception.UserException as e:
            return JsonResponse({'error': str(e), 'status': 400})
        except Exception as e:
            obj_logger = common_util.MyLogger(util.logs_directory, util.category)
            obj_logger.error_logger('forward_ltc : %s' % (str(e)))
            return JsonResponse({'error': exception_str.UserExceptionStr.bad_request, 'status': 400})
           


@csrf_exempt
@obj_common.who_is_hitting
@obj_common.valid_user
def peer_forward_ltc(request):
    if request.method == 'POST':
        try:
            # Get Request Data
            post_data = json.loads(request.body)
            user_name = post_data["user_name"]
            token = post_data["token"]
            input_address = post_data["input_address"]
            output_address_json = post_data["output_address"]

            # Server Side Checks
            common_util.check_if_present(user_name, token, input_address)
            util.validate_address(input_address)
            send_amount = 0.0
            print output_address_json
            for key, value in output_address_json.iteritems():
                util.validate_address(key)
                send_amount =  send_amount + float(value)


            util.validate_amount(input_address,send_amount)
            raw_transaction_hex,spent_tx_ids = util.create_raw_transaction(input_address,output_address_json)
            private_key =  util.key(input_address)
            # print private_key
            log = 'end_points'
            dec_private_key = common_util.AESCipher(token, log).decrypt(private_key)      
            signed_transaction = util.sign_raw_transaction(raw_transaction_hex,dec_private_key)
            txid =  util.send_raw_transaction(signed_transaction)
            util.update_spent_txids(spent_tx_ids)

            return JsonResponse({'txid': str(txid), 'status': 200})

        except custom_exception.UserException as e:
            print "------------"
            print e
            return JsonResponse({'error': str(e), 'status': 400})
        # except Exception as e:
        #     obj_logger = common_util.MyLogger(util.logs_directory, util.category)
        #     obj_logger.error_logger('forward_ltc : %s' % (str(e)))
        #     return JsonResponse({'error': exception_str.UserExceptionStr.bad_request, 'status': 400})
        print e
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + str(fname) + str(exc_tb.tb_lineno)
        print error_stack
        obj_logger.error_logger('createrawtransaction error : ' + str(error_stack))
        raise custom_exception.UserException(exception_str.UserExceptionStr.bad_request)


# # raw_transaction_hex,spent_tx_ids = util.create_raw_transaction(from_address,output_address_json)
# # private_key = common_util.config.get('forward_balance', 'private_key')  
# # log = 'end_points'
# dec_private_key = common_util.AESCipher(token, log).decrypt(private_key)      
# signed_transaction = util.sign_raw_transaction(raw_transaction_hex,dec_private_key)
# txid =  util.send_raw_transaction(signed_transaction)
# util.update_spent_txids(spent_tx_ids)

