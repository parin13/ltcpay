import sys, os
import datetime
import redis
from util import insert_sql,increment_sql,rpc_request,update_sql,send_notification,find_sql_join,config,MyLogger

# Redis Connection
pool = redis.ConnectionPool(
    host = config.get('redis', 'host'),
    port = int(config.get('redis', 'port')),
    db = int(config.get('redis', 'db'))
)
redis_conn = redis.Redis(connection_pool=pool)

# Blockchain Node
url = config.get('node', 'url')
confirmation_threshold = 6

logs_directory = config.get('block', 'logs')
category = config.get('block', 'category')
hook_queue = config.get('hook_main', 'queue')

def get_current_block(obj_logger):
    get_info = rpc_request(obj_logger,'getblockchaininfo',[])
    if not get_info:
        raise Exception('get_current_block | Node is Down')
    return int(get_info.get('result',{}).get('blocks'))


def get_block_data(obj_logger, number):

    # Get Block Hash
    block_hash = rpc_request(obj_logger,'getblockhash',[number])
    if not block_hash:
        raise Exception('get_block_data | Node is Down')
    block_hash =  block_hash.get('result')

    block_data = rpc_request(obj_logger,'getblock',[block_hash])
    if not block_data:
        raise Exception('get_block_data | Node is Down')
    block_data = block_data.get('result')

    return block_data


def get_raw_transaction(obj_logger, tx_id):
    tx_info = rpc_request(obj_logger, 'getrawtransaction', [tx_id, 1])
    if not tx_info:
        raise Exception('get_raw_transaction | Node is Down')
    return tx_info.get('result', [])


def process_address(obj_logger, to_address, tx_id, value, confirmations, block_number):
    """
    Add Transaction in DB and Send Notification
    :return: None
    """

    # Get Notification URL
    notification_url = find_sql_join(logger=obj_logger,
                                     table_names=['user_master', 'address_master'],
                                     filters={'address_master.address': to_address},
                                     on_conditions={'user_master.user_name': 'address_master.user_name'},
                                     columns=['user_master.notification_url']
                                     )[0]['notification_url']

    notification_data = {
        'tx_id': tx_id, 'address': to_address,
        'confirmations': confirmations, 'value': value
    }

    # Update Confirmation and Block Number
    status = update_sql(obj_logger, 'transaction_master', {'tx_id': tx_id},updated_values={'confirmations': confirmations, 'block_number': block_number})

    if status :
        send_notification(obj_logger,notification_url, notification_data, queue=hook_queue)

        # Removing from vmp_set, zct_set and adding in pct_set(for 1+ confirmation)
        redis_conn.srem('vmp_set', tx_id)
        redis_conn.srem('zct_set', tx_id)
        redis_conn.sadd('pct_set', tx_id)
    else:
        obj_logger.error_logger('>>>>>>>> Data not present in DB for : %s ' % (tx_id))


def block_crawler():
    """
    Block Crawling process
    :return:
    """
    obj_logger = MyLogger(logs_directory, category)
    obj_logger.msg_logger('Getting Block Numbers.....')

    # Get Current Block from RPC
    current_block = get_current_block(obj_logger)
    crawled_blocks = int(redis_conn.get('blocks_crawled') or 0)

    obj_logger.msg_logger('Crawled Block Number : %s'%(crawled_blocks))
    obj_logger.msg_logger('Current Block Number : %s' % (current_block))
    obj_logger.msg_logger('Pending : %s' % (current_block - crawled_blocks))

    if current_block > crawled_blocks:

        for block_number in range(crawled_blocks + 1, current_block + 1):

            obj_logger.msg_logger('Crawling Block : %s || Current Block : %s'%(block_number,current_block))
            obj_logger.msg_logger('Pending : %s' % (current_block - block_number))
            obj_logger.msg_logger('Start :%s'%(datetime.datetime.now()))

            # Increment Confirmations for tx_id whose 1+ confirmation is already sent
            for tx_id in redis_conn.smembers('pct_set'):

                data = find_sql_join(logger=obj_logger,
                    table_names=['user_master','address_master','transaction_master'],
                    filters = {'transaction_master.tx_id':tx_id},
                    on_conditions = {'user_master.user_name':'address_master.user_name','address_master.address':'transaction_master.to_address'},
                )

                if not data:
                    obj_logger.error_logger('>>>>>>>>>>> Data not found in SQL for tx_hash : %s'%(tx_id))
                    continue

                confirmations =  data[0]['confirmations']
                notification_url = data[0]['notification_url']

                if confirmations < confirmation_threshold:
                    status = increment_sql(obj_logger, 'transaction_master', {'tx_id':tx_id}, 'confirmations')
                    if status :
                        notification_data = {
                            'tx_id': tx_id,
                            'address': data[0]['to_address'],
                            'confirmations': confirmations + 1, # As already incremented in DB
                            'value': data[0]['value']
                        }
                        obj_logger.msg_logger('>>>>>>>> Sending Confirmation : %s || %s' % (confirmations + 1, tx_id))
                        send_notification(obj_logger, notification_url, notification_data, queue=hook_queue)
                    else:
                        obj_logger.error_logger('>>>>>>>> Data not present in DB for  : %s ' % (tx_id))
                else:
                    obj_logger.msg_logger('>>>>>>>> %s Confirmation Sent : %s' %(confirmation_threshold,  tx_id))
                    obj_logger.msg_logger('>>>>>>>> Removing from pct_set : %s' %(tx_id))
                    redis_conn.srem('pct_set', tx_id)

            # Crawling Blocks
            block_data = get_block_data(obj_logger, block_number)
            block_transactions = block_data.get('tx')

            for tx_id in block_transactions:

                # If not present in zct_set, means that it is not being catched in mempool and hence no need to be proccessed
                if not redis_conn.sismember('zct_set', tx_id) :
                    continue

                # If present in pct_set , means 1 confirmation is already sent
                if redis_conn.sismember('pct_set', tx_id) :
                    obj_logger.msg_logger('Confirmation 1 already sent for tx : %s' % (tx_id))
                    continue

                tx_data = get_raw_transaction(obj_logger, tx_id)
                vouts = tx_data['vout']
                confirmations = int(tx_data['confirmations'])
                confirmations = confirmation_threshold if confirmations > confirmation_threshold else confirmations

                for vout in vouts:
                    script_pub_key = vout['scriptPubKey']
                    _type = script_pub_key['type']

                    if not _type:
                        obj_logger.error_logger('Server Error : Unknown Type : %s' % (tx_id))

                    if _type in ['pubkeyhash', 'scripthash']:
                        to_address = script_pub_key['addresses'][0]
                        value = vout['value']

                        # Check if the address needs to be processed
                        if redis_conn.sismember('aw_set', to_address):
                            obj_logger.msg_logger('>>>>>>>> Transaction Found in Block : %s' % (tx_id))
                            process_address(obj_logger, to_address, tx_id, value, confirmations, block_number)

            # Increment Redis Blocks Crawled
            redis_conn.set('blocks_crawled',block_number)
            obj_logger.msg_logger('Ends :%s' % (datetime.datetime.now()))

def main():
    """
    Scheduling
    :return:
    """
    try:
        block_crawler()
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print  error_stack
        obj_logger = MyLogger(logs_directory, category)
        obj_logger.error_logger('Main : %s' % (error_stack))


if __name__ == '__main__':
    main()
