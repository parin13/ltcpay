import sys, os
import datetime
import redis
from util import insert_sql, rpc_request, send_notification, find_sql_join, MyLogger, config

# Redis Connection
pool = redis.ConnectionPool(
    host = config.get('redis', 'host'),
    port = int(config.get('redis', 'port')),
    db = int(config.get('redis', 'db'))
)
redis_conn = redis.Redis(connection_pool=pool)

# Blockchain Node
logs_directory = config.get('mempool', 'logs')
category = config.get('mempool', 'category')
hook_queue = config.get('hook_main', 'queue')


def process_address(obj_logger, to_address, tx_id, value, index, confirmations, _type, asm, _hex):
    """
    Add Transaction in DB and Send Notification
    :return: None
    """

    notification_url = find_sql_join(logger=obj_logger,
                                     table_names=['user_master', 'address_master'],
                                     filters={'address_master.address': to_address},
                                     on_conditions={'user_master.user_name': 'address_master.user_name'},
                                     columns=['user_master.notification_url']
                                     )[0]['notification_url']
    bid_id = -1
    block_number = -2
    spent_flag = 0

    data = {
        'tx_id': tx_id,
        'to_address': to_address,
        'vout': index,
        'bid_id': bid_id,
        'confirmations': confirmations,
        'block_number': block_number,
        'value': value,
        'spent_flag': spent_flag,
        'sys_timestamp': datetime.datetime.now(),
        'type': _type,
        'asm': asm,
        'hex': _hex,
    }

    success = insert_sql(
        logger = obj_logger,
        table_name = 'transaction_master',
        data = data
    )

    if success:
        redis_conn.sadd('zct_set', tx_id)
        notification_data = {
            'tx_id': tx_id, 'address': to_address,
            'confirmations': confirmations, 'value': value
        }
        send_notification(obj_logger, notification_url, notification_data, queue=hook_queue)


def mempool_crawler():
    """
    Mempool Process
    :return:
    """
    obj_logger = MyLogger(logs_directory,category)
    obj_logger.msg_logger('Getting Mempool Data')

    # Get Mempool Transactions
    list_transactions = rpc_request(obj_logger, 'getrawmempool', [])

    if not list_transactions:
        raise Exception('Litecoin Node is down')

    list_transactions = list_transactions.get('result', [])

    obj_logger.msg_logger('Crawling Mempool Starts')

    for tx_id in list_transactions:

        # Check if already checked
        if redis_conn.sismember('vmp_set',tx_id):
            obj_logger.msg_logger('Transaction already checked : %s' %(tx_id))
            continue

        # Get Transaction Data
        tx_data = rpc_request(obj_logger, 'getrawtransaction',[tx_id, 1]).get('result',{})
        if not tx_data:
            obj_logger.error_logger('Mempool Data not found for tx_hash : %s'%(tx_id))
            continue

        vouts = tx_data.get('vout',{})

        for vout in vouts:
            script_pub_key = vout['scriptPubKey']
            _type = script_pub_key['type']

            if not _type:
                obj_logger.error_logger('Server Error : Unknown Type : %s' % (tx_id))

            if _type in ['pubkeyhash', 'scripthash']:
                to_address = script_pub_key['addresses'][0]
                value = str(vout['value'])
                n_index = vout['n']
                _hex = script_pub_key['hex']

                # Check if the address needs to be processed
                if redis_conn.sismember('aw_set', to_address):
                    obj_logger.msg_logger('>>>>>>>> Transaction Found in Mempool : %s' % (tx_id))
                    asm = script_pub_key['asm']
                    confirmations = 0
                    process_address(obj_logger, to_address, tx_id, value, n_index, confirmations, _type, asm, _hex)

        redis_conn.sadd('vmp_set', tx_id)
        obj_logger.msg_logger('Crawling Mempool Ends')


def main():
    """
    Start Mempool
    :return:
    """
    try:
        mempool_crawler()
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_stack = str(exc_type) + ' | ' + str(fname) + ' | ' + str(exc_tb.tb_lineno) + ' | ' + str(e)
        print  error_stack
        obj_logger = MyLogger(logs_directory,category)
        obj_logger.error_logger('Main : %s'%(error_stack))

if __name__ == '__main__':
    main()
