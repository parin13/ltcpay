
Litecoin Pytment Gateway
================================
Copyright (c) Parishilan Rayamajhi/ parishilanrayamajhi@gmail.com <br/>


What is ltc pay?
----------------

ltc pay is a enterprise ready litecoin payment gateway system compactable for exchagne paltform , wallet application and relevant application.
Main feature provided by ltc pay are:
 - private key management system
 - supports inhouse node setup 
 - No dependency with third party
 - Seperate crawler for crawling blockchain
 - Supports multisig wallet
 - hd wallet in under developement phase
 




Development process
-------------------
The stack used for the developement process are :

 - Python 2.*
 - Django 2.7
 - Mysql 
 - Redis
 - RabbitMq
 - Apache2
 - Nodejs



The `release` & `phase2` branch is regularly built and tested, but is not guaranteed to be
completely stable.  Developement work on `master ` branch has been halted with the purpose of making master as generic code base for further currencyes


Testing
-------

Testing and code review is the bottleneck for development; Any pull request and contribution is most welcomed,
but remember this is a security-critical project where any mistake might cost people
lots of money.



### using ltc pay  in ubuntu system
  
To make the project hands on for maximum users i have provided seperate scripts for installing database schemas and all the required service
  1) database script : ``` root/Servers/create_db_script.py ```
  2) creating log folders :  ``` root/Servers/create_log_folders.sh ```
  3) installing apache : ``` root/Servers/install_apache.sh ```
  4) installing nodejs : ``` root/Servers/install_nodejs_npm.sh ```
  5) install python packages : ``` root/Servers/install_python_pip_and_packages.sh ```
  6) installing rabbitmq: ``` root/Servers/install_rabbitmq.sh ```
  7) installing redis : ``` root/Servers/install_redis.sh```
 
