#!/bin/bash
echo "*******************************************************************************************"
echo "*******************************************************************************************"
echo "******************************** Synchronizing the package index files.... ****************"
echo "********************************        OR                  *******************************"
echo "******************************** Doing apt update..... ************************************"
echo "*******************************************************************************************"
sudo apt update
sudo ./install_sql_server_script.sh
sudo ./install_python_pip_and_packages.sh
sudo python2.7 ./create_db_script.py
sudo ./install_redis.sh
sudo ./install_rabbitmq.sh
sudo ./install_apache.sh
sudo ./install_nodejs_npm.sh
sudo ./install_pm2.sh
sudo ./create_log_folders.sh
cd /var/www/ltcpay/node_apps/
sudo npm install
echo "*******************************************************************************************"
echo "********************************* DONE!! **************************************************"
echo "*******************************************************************************************"
# End