#!/bin/bash
echo "*******************************************************************************************"
echo "*******************************************************************************************"
echo "******************************** Server Up Script......... ********************************"
echo "*******************************************************************************************"
echo "******************************** Starting mysql-server......... ***************************"
sudo service mysql start
echo "******************************** Starting redis-server......... ***************************"
sudo service redis-server start
echo "******************************** Starting rabbitmq-server......... ************************"
sudo service rabbitmq-server start
echo "******************************** Starting apache2 server......... *************************"
sudo service apache2 start
echo "******************************** Starting nodejs server using pm2......... ****************"
sudo pm2 start /var/www/ltcpay/node_apps/generate_address.js
echo "To run Crawlers do following manually:"
echo "crontab -e"
echo "copy these 3 lines to crontab"
echo "*/1 * * * * python2.7 /var/www/ltcpay/Crawlers/mempool_crawler.py"
echo "*/1 * * * * python2.7 /var/www/ltcpay/Crawlers/hook_consumer.py"
echo "*/1 * * * * python2.7 /var/www/ltcpay/Crawlers/block_crawler.py"
echo "write into default crontab file and exit"
echo "*******************************************************************************************"
echo "*******************************************************************************************"
echo "*******************************************************************************************"
echo "*******************************************************************************************"