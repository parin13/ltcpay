import MySQLdb

# Connects to your local MySQL instance
print "Connecting to MySQL....."
db = MySQLdb.connect(host="localhost",user="root",passwd="root")
cursor = db.cursor()
print "Creating database ltc....."
create_db = 'CREATE DATABASE IF NOT EXISTS ltc'
cursor.execute(create_db)

# Sets the database as current where all the further queries will be executed
print "Using newly created database ltc\n"
use_db = 'USE ltc'
cursor.execute(use_db)

# This table stores the User Master data for validating users
create_table_user_master = '''CREATE TABLE IF NOT EXISTS `user_master` (
  `user_name` varchar(100) UNIQUE NOT NULL,
  `token` varchar(500) DEFAULT NULL,
  `notification_url` varchar(500) NOT NULL,
  `app_key` varchar(50) DEFAULT NULL,
  `app_secret` varchar(50) DEFAULT NULL,
  `timestamp` datetime(6),
  PRIMARY KEY (`user_name`)
)'''

# This table stores the data for generated address
create_table_address_master = '''CREATE TABLE IF NOT EXISTS `address_master` (
  `address` varchar(50) UNIQUE NOT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `public_key` varchar(200) DEFAULT NULL,
  `private_key` varchar(500) DEFAULT NULL,
  `timestamp` datetime(6),
  PRIMARY KEY (`address`),
  FOREIGN KEY (`user_name`) REFERENCES user_master(`user_name`)
)'''

# This table stores the data for all the LTC transactions
create_table_transaction_master = '''CREATE TABLE IF NOT EXISTS `transaction_master` (
  `tx_id` varchar(500) NOT NULL,
  `to_address` varchar(500) DEFAULT NULL,
  `vout` varchar(10) DEFAULT NULL,
  `bid_id` varchar(100) DEFAULT NULL,
  `confirmations` int(11) DEFAULT NULL,
  `block_number` int(11) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL,
  `spent_flag` TINYINT DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `asm` varchar(100) DEFAULT NULL,
  `hex` varchar(100) DEFAULT NULL,
  `block_timestamp` datetime(6),
  `sys_timestamp` datetime(6)
)'''

# This table stores the log data of any error occuring in the python scripts
create_table_error_logs_master = '''CREATE TABLE IF NOT EXISTS `error_logs` (
  `error_id` int(11) UNIQUE NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `error` varchar(1000) DEFAULT NULL,
  `meta` varchar(1000) DEFAULT NULL,
  `timestamp` datetime(6),
  PRIMARY KEY (`error_id`)
)'''

# This table stores the log data of all the requests that are coming to the server
create_table_server_logs_master = '''CREATE TABLE IF NOT EXISTS `server_logs` (
  `request_id` int(11) UNIQUE NOT NULL AUTO_INCREMENT,
  `request_ip` varchar(100) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  `headers` varchar(500) DEFAULT NULL,
  `body` varchar(500) DEFAULT NULL,
  `response` varchar(500) DEFAULT NULL,
  `timestamp` datetime(6),
  PRIMARY KEY (`request_id`)
)'''

# This table stores the log data for all the notification sent to the users
create_table_notification_logs_master = '''CREATE TABLE IF NOT EXISTS `notification_logs` (
  `id` int(11) UNIQUE NOT NULL AUTO_INCREMENT,
  `tx_hash` varchar(500) DEFAULT NULL,
  `notification_url` varchar(500) DEFAULT NULL,
  `params` varchar(500) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `timestamp` datetime(6),
  PRIMARY KEY (`id`)
)'''

print "Creating table user_master"
cursor.execute(create_table_user_master)
print "DONE\n"
print "Creating table address_master"
cursor.execute(create_table_address_master)
print "DONE\n"
print "Creating table transactions_master"
cursor.execute(create_table_transaction_master)
print "DONE\n"
print "Creating table error_logs"
cursor.execute(create_table_error_logs_master)
print "DONE\n"
print "Creating table server_logs"
cursor.execute(create_table_server_logs_master)
print "DONE\n"
print "Creating table notification_logs"
cursor.execute(create_table_notification_logs_master)
print "\nDB Created Successfully"