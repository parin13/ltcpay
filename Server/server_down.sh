#!/bin/bash
echo "*******************************************************************************************"
echo "*******************************************************************************************"
echo "******************************** Server Down Script......... ******************************"
echo "*******************************************************************************************"
echo "******************************** Stopping mysql-server......... ***************************"
sudo service mysql stop
echo "******************************** Stopping redis-server......... ***************************"
sudo service redis-server stop
echo "******************************** Stopping rabbitmq-server......... ************************"
sudo service rabbitmq-server stop
echo "******************************** Stopping apache2 server......... *************************"
sudo service apache2 stop
echo "******************************** Stopping nodejs server using pm2......... ****************"
sudo pm2 stop generate_address
echo "******************************** Stopping cron jobs......... ******************************"
sudo crontab -r
echo "*******************************************************************************************"
echo "*******************************************************************************************"
echo "************************************* DONE ************************************************"
echo "*******************************************************************************************"